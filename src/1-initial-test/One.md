Run using `yarn test:watch One.exercise`

# One

This test will get you used to creating simple tests in React.

You'll learn to set up a test file and test a very simple component in React.

The component has a heading and paragraph.

## Task

~Render~ the component in the test environment

~Query~ to check whether or not the correct ~text~ is appearing.

There are two HTML elements being displayed, an H1, and a P.

[Cheat Sheet](https://testing-library.com/docs/react-testing-library/cheatsheet)
