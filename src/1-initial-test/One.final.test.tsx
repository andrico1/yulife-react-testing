import React from "react";
import { render } from "@testing-library/react";
import One from "./One";

function renderComponent() {
  return render(<One />);
}

describe("One", () => {
  it("should display the correct heading", () => {
    const { queryByText } = renderComponent();

    expect(queryByText("Hello World")).toBeTruthy();
  });

  it("should display the correct text content", () => {
    const { queryByText } = renderComponent();

    expect(queryByText("Some more text")).toBeTruthy();
  });
});
