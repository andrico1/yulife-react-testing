import React from "react";
import { RouteComponentProps } from "@reach/router";

function One(props: RouteComponentProps) {
  return (
    <div>
      <h1>Hello World</h1>
      <p>Some more text</p>
    </div>
  );
}

export default One;
