import React from "react";
import { RouteComponentProps, Link } from "@reach/router";

function Nav(props: RouteComponentProps) {
  return (
    <nav style={{ display: "flex", flexDirection: "row" }}>
      <Link to="/one">One</Link>
      <Link to="/two">Two</Link>
      <Link to="/three">Three</Link>
    </nav>
  );
}

export default Nav;
