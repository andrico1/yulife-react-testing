import React from "react";
import { Router } from "@reach/router";
import Nav from "./Nav";
import One from "./1-initial-test/One";
import Two from "./2-interactions/Two";
import Three from "./3-forms/Three";
import "./App.css";

function App() {
  return (
    <>
      <Nav />
      <Router>
        <One path="/one" />
        <Two path="/two" text="Button" onClick={() => null} />
        <Three path="/three" onSubmit={() => null} />
      </Router>
    </>
  );
}

export default App;
