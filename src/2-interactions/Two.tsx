import React from "react";
import { RouteComponentProps } from "@reach/router";

interface Props extends RouteComponentProps {
  text: string;
  onClick: () => void;
}

function Button(props: Props) {
  return <button onClick={props.onClick}>{props.text}</button>;
}

export default Button;
