import React, { useState } from "react";
import { render, fireEvent } from "@testing-library/react";
import Two from "./Two";
import Button from "./Two";

const mockHandler = jest.fn();

function renderComponentOne() {
  return render(<Two text="Button" onClick={mockHandler} />);
}

function renderComponentTwo() {
  let counter = 0;

  function getCounter() {
    return counter;
  }

  function updateCounter() {
    counter++;
  }

  const api = render(<Two text="Button" onClick={updateCounter} />);

  return {
    ...api,
    getCounter,
  };
}

interface ParentContainerProps {
  children: (handleCLick: () => void) => React.ReactNode;
}

function renderComponentThree() {
  function ParentContainer(props: ParentContainerProps) {
    const [counter, updateCounter] = useState(0);

    function handleClick() {
      updateCounter(counter + 1);
    }

    return (
      <>
        <p data-testid="parent-container">{counter}</p>
        {props.children(handleClick)}
      </>
    );
  }

  return render(
    <ParentContainer>
      {(handleClick) => {
        return <Button text="Button" onClick={handleClick} />;
      }}
    </ParentContainer>
  );
}

describe("Two", () => {
  it("should fire the click handler when the button is clicked", () => {
    jest.resetAllMocks();

    const { getByText } = renderComponentOne();

    expect(mockHandler).toHaveBeenCalledTimes(0);

    fireEvent.click(getByText("Button"));

    expect(mockHandler).toHaveBeenCalledTimes(1);
  });

  it("should update the counter when the button is clicked", () => {
    const { getCounter, getByText } = renderComponentTwo();

    expect(getCounter()).toBe(0);

    fireEvent.click(getByText("Button"));

    expect(getCounter()).toBe(1);
  });

  it("should update the parent container when the button is clicked", () => {
    const { getByTestId, getByText } = renderComponentThree();

    expect(getByTestId("parent-container")).toHaveTextContent("0");

    fireEvent.click(getByText("Button"));

    expect(getByTestId("parent-container")).toHaveTextContent("1");
  });
});
