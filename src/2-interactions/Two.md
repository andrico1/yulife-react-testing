Run using `yarn test:watch Two.exercise`

# Two

This test will get you used to testing interactions.

You'll render a button component that you will then interact with.

The component is a button that recieved a click handler.

## Task

~Render~ the component in the test environment

~Get~ the button by its text value

~Fire a click event~ on the button

Make an assertion that the click handler fired correctly.

Tip: `jest.fn()` let's you run assertions to test that it was fire correctly. 

Note:

There are a number of ways to evaluate that the button worked correctly. Here thay are in order of ascending complexity

- Use jest.fn() to mock the onClick handler
- Creating a counter that updates when the button is clicked, the value is then added to `renderComponent()`'s return object
- Create a Children-as-a-function test component that exposes the event handler that responds when clicked

[Cheat Sheet](https://testing-library.com/docs/react-testing-library/cheatsheet)
