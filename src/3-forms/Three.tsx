import React, { useState } from "react";
import { RouteComponentProps } from "@reach/router";

interface Props extends RouteComponentProps {
  onSubmit: (formState: {}) => void;
}

function Three(props: Props) {
  const [formState, setFormState] = useState({
    name: "",
    email: "",
    select: "",
  });

  function handleInputChange(key: string, value: string) {
    setFormState({
      ...formState,
      [key]: value,
    });
  }

  return (
    <form
      style={{ display: "flex", flexDirection: "column", width: "300px" }}
      data-testid="form"
      onSubmit={(e) => {
        e.preventDefault();
        props.onSubmit(formState);
      }}
    >
      <label htmlFor="name">Name:</label>
      <input
        onChange={({ target }) => handleInputChange("name", target.value)}
        id="name"
        type="text"
      />
      <input
        onChange={({ target }) => handleInputChange("email", target.value)}
        type="text"
        placeholder="Enter your email:"
      />
      <label htmlFor="select">Choose an option:</label>
      <select
        onChange={({ target }) => handleInputChange("select", target.value)}
        id="select"
      >
        <option value="One">One</option>
        <option value="Two">Two</option>
        <option value="Three">Three</option>
      </select>
      <button type="submit">Submit</button>
    </form>
  );
}

export default Three;
