import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Three from "./Three";

const mockedOnSubmit = jest.fn();

function renderComponent() {
  return render(<Three onSubmit={mockedOnSubmit} />);
}

describe("Three", () => {
  it("should successfully submit the form", () => {
    const {
      getByPlaceholderText,
      getByLabelText,
      getByTestId,
    } = renderComponent();

    fireEvent.change(getByLabelText("Name:"), {
      target: { value: "Hey there" },
    });

    fireEvent.change(getByPlaceholderText("Enter your email:"), {
      target: { value: "myemail@test.com" },
    });

    fireEvent.change(getByLabelText("Choose an option:"), {
      target: { value: "Two" },
    });

    fireEvent.submit(getByTestId("form"));

    expect(mockedOnSubmit).toHaveBeenCalledWith({
      email: "myemail@test.com",
      name: "Hey there",
      select: "Two",
    });
  });
});
