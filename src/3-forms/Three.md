Run using `yarn test:watch Three.exercise`

# Three

This test will get you used to forms and more complex user interactions.

You'll render a component with a few fields, and then submit the function

## Task

~Render~ the component in the test environment

~Get~ each input by its label OR placeholder

~Fire a change event~ on the input

~Fire a submit event~ on the form

Check that the form was submitted with the correct form data

Note: When firing a `change` even with the testing library, the first argument is that reference to the element that you want to change. The second argument is `event` object. This mimics the argument that is passed through to the `onChange` event handler. For example:

```typescript
const emailInput = getByLabel("email");
const changeEvent = { target: { value: "fake@123.com" } };

fireEvent.change(emailInput, changeEvent);
```

[Cheat Sheet](https://testing-library.com/docs/react-testing-library/cheatsheet)
