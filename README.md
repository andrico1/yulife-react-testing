
## Available Scripts

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Using this tutorial

Each section of the tutorial has its own directory, as well as it's own route in the app.

By accessing it's corresponding route in the app, you cansee how the components display. Aside from looking at the component, you shouldn't need to do much else inside of the browser

In its directory you'll find 4 files:

- A readme that contains the instructions for the section
- The `*.tsx` component which is the component being tested
- The `*.exercise.test.tsx` file which is the test file you should attempt to write
- The `*.final.test.tsx` file which contains one or more solutions to the section


